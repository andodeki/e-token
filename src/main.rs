#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![deny(clippy::clone_on_ref_ptr)]

use embassy_executor::Spawner;
// use embassy_futures::select::{select, Either};
use embassy_net::{Config, ConfigV4, Ipv4Address, Ipv4Cidr, Stack, StackResources};
use embassy_net_ppp::Runner;
use embassy_sync::{blocking_mutex::raw::NoopRawMutex, mutex::Mutex, signal::Signal};
use embassy_time::{with_timeout, Duration, Timer};
use esp_backtrace as _;
use hal::{
    clock::{ClockControl, Clocks},
    delay::Delay,
    gpio::{GpioPin, Io},
    peripherals::{Peripherals, UART0, UART1},
    prelude::*,
    system::SystemControl,
    timer::{timg::TimerGroup, ErasedTimer, OneShotTimer, PeriodicTimer},
    uart::{
        config::{Config as UartConfig, DataBits, Parity, StopBits},
        ClockSource, Uart,
    },
    Async,
};
use static_cell::StaticCell;

extern crate alloc;
use core::mem::MaybeUninit;

use e_token::{modem::modem_managers::ModemManager, utils::generate_random_seed};

#[global_allocator]
static ALLOCATOR: esp_alloc::EspHeap = esp_alloc::EspHeap::empty();

fn init_heap() {
    const HEAP_SIZE: usize = 32 * 1024;
    static mut HEAP: MaybeUninit<[u8; HEAP_SIZE]> = MaybeUninit::uninit();

    unsafe {
        ALLOCATOR.init(HEAP.as_mut_ptr() as *mut u8, HEAP_SIZE);
    }
}
// When you are okay with using a nightly compiler it's better to use https://docs.rs/static_cell/2.1.0/static_cell/macro.make_static.html
macro_rules! mk_static {
    ($t:ty,$val:expr) => {{
        static STATIC_CELL: static_cell::StaticCell<$t> = static_cell::StaticCell::new();
        #[deny(unused_attributes)]
        let x = STATIC_CELL.uninit().write(($val));
        x
    }};
}
// #[entry]
#[esp_hal_procmacros::main]
async fn main(spawner: Spawner) {
    let peripherals = Peripherals::take();
    let system = SystemControl::new(peripherals.SYSTEM);

    let clocks = ClockControl::max(system.clock_control).freeze();
    let delay = Delay::new(&clocks);
    init_heap();

    esp_println::logger::init_logger_from_env();

    // let timer = hal::timer::PeriodicTimer::new(
    //     hal::timer::timg::TimerGroup::new(peripherals.TIMG1, &clocks)
    //         .timer0
    //         .into(),
    // );

    // let timg0 = TimerGroup::new(peripherals.TIMG0, &clocks, None);
    let timg0 = TimerGroup::new(peripherals.TIMG0, &clocks);
    let timer0: ErasedTimer = timg0.timer0.into();
    let timer = PeriodicTimer::new(timer0);

    let _init = esp_wifi::initialize(
        esp_wifi::EspWifiInitFor::Wifi,
        timer,
        hal::rng::Rng::new(peripherals.RNG),
        peripherals.RADIO_CLK,
        &clocks,
    )
    .unwrap();

    #[cfg(feature = "esp32")]
    {
        // let timg1 = TimerGroup::new(peripherals.TIMG1, &clocks, None);
        let timg1 = TimerGroup::new(peripherals.TIMG1, &clocks);
        let timer0: ErasedTimer = timg1.timer0.into();
        let timers = [OneShotTimer::new(timer0)];
        let timers = mk_static!([OneShotTimer<ErasedTimer>; 1], timers);
        esp_hal_embassy::init(&clocks, timers);
    }

    // let timers = [OneShotTimer::new(timer0)];
    // let timers = mk_static!([OneShotTimer<ErasedTimer>; 1], timers);
    // esp_hal_embassy::init(&clocks, timers);

    let io = Io::new(peripherals.GPIO, peripherals.IO_MUX);

    let config = UartConfig {
        baudrate: 115200,
        data_bits: DataBits::DataBits8,
        parity: Parity::ParityNone,
        stop_bits: StopBits::STOP1,
        clock_source: ClockSource::Apb,
        rx_fifo_full_threshold: 1,
        rx_timeout: Some(3),
    };

    config.rx_fifo_full_threshold(1);
    #[cfg(feature = "esp32")]
    let (tx_pin, rx_pin) = (io.pins.gpio14, io.pins.gpio15);

    // let mut uart0 =
    //     Uart::new_async_with_config(peripherals.UART0, config, &clocks, tx_pin, rx_pin).unwrap();

    let uart: Uart<'static, UART1, Async> =
        Uart::new_async_with_config(peripherals.UART1, config, &clocks, tx_pin, rx_pin).unwrap();

    // let mut modem_manager = ModemManager::new(uart);
    // modem_manager.configure_connection().await.unwrap();

    // Init network device
    static STATE: StaticCell<embassy_net_ppp::State<4, 4>> = StaticCell::new();
    let state = STATE.init(embassy_net_ppp::State::<4, 4>::new());
    let (device, runner) = embassy_net_ppp::new(state);

    // Generate random seed
    let seed = generate_random_seed();

    // Init network stack
    static STACK: StaticCell<Stack<embassy_net_ppp::Device<'static>>> = StaticCell::new();
    static RESOURCES: StaticCell<StackResources<3>> = StaticCell::new();

    let stack = &*STACK.init(Stack::new(
        device,
        Config::default(), // don't configure IP yet
        RESOURCES.init(StackResources::<3>::new()),
        seed,
    ));
    loop {
        // modem_manager.check_and_handle_sms().await.unwrap();
        Timer::after(Duration::from_secs(5)).await;
    }
}
