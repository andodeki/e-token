#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![deny(clippy::clone_on_ref_ptr)]
extern crate alloc;

pub mod modem;
pub mod utils;
