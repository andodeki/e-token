use core::{
    fmt,
    sync::atomic::{AtomicBool, Ordering},
};

use alloc::{
    format,
    string::{String, ToString},
    vec::Vec,
};
use embassy_futures::select::select;
use embassy_time::{Duration, Instant, Timer};
use embedded_io_async::{BufRead, Read, Write};
use log::info;

pub struct ModemManager<T: Read + Write + BufRead> {
    pub io: ModemIO<T>,
    ppp_interval: Duration,
    last_ppp_time: Instant,
}

impl<T: Read + Write + BufRead> ModemManager<T> {
    pub fn new(uart: T) -> Self {
        Self {
            io: ModemIO::new(uart),
            ppp_interval: Duration::from_secs(3600), // Default to 1 hour
            last_ppp_time: Instant::now(),
        }
    }
    async fn send_at_command(&mut self, command: &str) -> Result<String, T::Error> {
        self.io.ensure_command_mode().await?;
        self.io.inner.write_all(command.as_bytes()).await?;
        self.io.inner.write_all(b"\r\n").await?;

        let mut response = String::new();
        let mut buf = [0u8; 64];
        loop {
            match self.io.inner.read(&mut buf).await {
                Ok(0) => break,
                Ok(n) => {
                    response.push_str(core::str::from_utf8(&buf[..n]).unwrap_or(""));
                    if response.contains("OK") || response.contains("ERROR") {
                        break;
                    }
                }
                Err(e) => return Err(e),
            }
        }
        Ok(response)
    }

    pub async fn configure_connection(&mut self) -> Result<(), T::Error> {
        self.send_at_command("AT+CGATT=1").await?;
        self.send_at_command("AT+CGDCONT=1,\"IP\",\"cmnet\"")
            .await?;
        self.send_at_command("AT+CGACT=1,1").await?;
        self.send_at_command("AT+CMGF=1").await?;
        Ok(())
    }
    async fn run(&mut self) -> Result<(), T::Error> {
        self.configure_connection().await?;

        loop {
            // select(self.check_and_handle_sms(), self.periodic_ppp_session()).await;

            // Wait for a short period before the next iteration
            Timer::after(Duration::from_secs(1)).await;
        }
    }
    async fn send_sms(&mut self, recipient: &str, message: &str) -> Result<(), T::Error> {
        self.send_at_command(&format!("AT+CMGS=\"{}\"", recipient))
            .await?;
        Timer::after(Duration::from_millis(100)).await;
        self.io.inner.write_all(message.as_bytes()).await?;
        self.io.inner.write_all(&[0x1A]).await?; // Ctrl+Z to send the message
        Timer::after(Duration::from_secs(1)).await;

        // Wait for the "OK" response
        let mut response = String::new();
        let mut buf = [0u8; 64];
        loop {
            match self.io.inner.read(&mut buf).await {
                Ok(0) => break,
                Ok(n) => {
                    response.push_str(core::str::from_utf8(&buf[..n]).unwrap_or(""));
                    if response.contains("OK") || response.contains("ERROR") {
                        break;
                    }
                }
                Err(e) => return Err(e),
            }
        }

        if response.contains("OK") {
            Ok(())
        } else {
            // Err(T::Error::from("Failed to send SMS"))
            Ok(())
        }
    }

    async fn periodic_ppp_session(&mut self) -> Result<(), T::Error> {
        if Instant::now().duration_since(self.io.last_ppp_session) >= self.io.ppp_interval {
            info!("Starting periodic PPP session");
            self.start_ppp().await?;

            // Run PPP for a fixed duration (e.g., 5 minutes)
            Timer::after(Duration::from_secs(300)).await;

            // Exit PPP mode
            self.exit_ppp().await?;

            self.io.last_ppp_session = Instant::now();
        }
        Ok(())
    }
    pub async fn check_and_handle_sms(&mut self) -> Result<(), T::Error> {
        if self.io.new_sms_flag.load(Ordering::Relaxed) {
            self.io.new_sms_flag.store(false, Ordering::Relaxed);
            self.handle_incoming_sms().await?;
        }
        Ok(())
    }

    async fn handle_incoming_sms(&mut self) -> Result<(), T::Error> {
        let response = self.send_at_command("AT+CMGL=\"REC UNREAD\"").await?;

        // Parse the response to extract SMS details
        for line in response.lines() {
            if line.starts_with("+CMGL:") {
                let parts: Vec<&str> = line.split(',').collect();
                if parts.len() >= 3 {
                    let index = parts[0].split(':').nth(1).unwrap().trim();
                    let sender = parts[2].trim_matches('"');

                    // Read the message content (it's in the next line)
                    if let Some(content) = response
                        .lines()
                        .nth(response.lines().position(|l| l == line).unwrap() + 1)
                    {
                        info!("New SMS from {}: {}", sender, content);

                        // Process the SMS content here
                        self.process_sms_command(sender, content).await?;

                        // Delete the processed message
                        self.send_at_command(&format!("AT+CMGD={}", index)).await?;
                    }
                }
            }
        }
        Ok(())
    }

    pub async fn start_ppp(&mut self) -> Result<(), T::Error> {
        self.io.enter_data_mode().await
    }
    pub async fn exit_ppp(&mut self) -> Result<(), T::Error> {
        // Send "+++" to exit PPP mode
        self.io.inner.write_all(b"+++").await?;
        Timer::after(Duration::from_secs(1)).await;

        // Confirm we're back in command mode
        self.send_at_command("AT").await?;

        self.io.state = ModemState::CommandMode;
        Ok(())
    }
    async fn process_sms_command(&mut self, sender: &str, content: &str) -> Result<(), T::Error> {
        match content.trim().to_lowercase().as_str() {
            "status" => {
                let status = self.get_connection_status().await?;
                self.send_sms(sender, &status).await?;
            }
            "restart" => {
                self.send_sms(sender, "Restarting modem...").await?;
                self.restart_modem().await?;
            }
            "ip" => {
                let ip = self.get_ip_address().await?;
                self.send_sms(sender, &format!("Current IP: {}", ip))
                    .await?;
            }
            "signal" => {
                let signal = self.get_signal_strength().await?;
                self.send_sms(sender, &format!("Signal strength: {}", signal))
                    .await?;
            }
            _ => {
                self.send_sms(
                    sender,
                    "Unknown command. Available commands: status, restart, ip, signal",
                )
                .await?;
            }
        }
        Ok(())
    }
    async fn get_connection_status(&mut self) -> Result<String, T::Error> {
        let response = self.send_at_command("AT+CREG?").await?;
        if response.contains("+CREG: 0,1") || response.contains("+CREG: 0,5") {
            Ok("Connected".to_string())
        } else {
            Ok("Not connected".to_string())
        }
    }

    async fn restart_modem(&mut self) -> Result<(), T::Error> {
        self.send_at_command("AT+CFUN=1,1").await?;
        Timer::after(Duration::from_secs(10)).await;
        self.configure_connection().await
    }

    async fn get_ip_address(&mut self) -> Result<String, T::Error> {
        let response = self.send_at_command("AT+CGPADDR=1").await?;
        if let Some(ip) = response.lines().find(|line| line.starts_with("+CGPADDR:")) {
            Ok(ip
                .split(',')
                .nth(1)
                .unwrap_or("Unknown")
                .trim_matches('"')
                .to_string())
        } else {
            Ok("Unknown".to_string())
        }
    }

    async fn get_signal_strength(&mut self) -> Result<String, T::Error> {
        let response = self.send_at_command("AT+CSQ").await?;
        if let Some(csq) = response.lines().find(|line| line.starts_with("+CSQ:")) {
            let parts: Vec<&str> = csq.split(':').nth(1).unwrap().split(',').collect();
            if let Some(rssi) = parts.first() {
                let rssi_val: i32 = rssi.trim().parse().unwrap_or(-1);
                if rssi_val >= 0 && rssi_val <= 31 {
                    let dbm = -113 + (2 * rssi_val);
                    Ok(format!("{} dBm", dbm))
                } else if rssi_val == 99 {
                    Ok("Unknown".to_string())
                } else {
                    Ok("Invalid".to_string())
                }
            } else {
                Ok("Unknown".to_string())
            }
        } else {
            Ok("Unknown".to_string())
        }
    }
    pub async fn handle_server_payload(&mut self, payload: &str) -> Result<(), T::Error> {
        if let Some(interval) = payload.strip_prefix("SET_PPP_INTERVAL:") {
            if let Ok(seconds) = interval.parse::<u64>() {
                let new_interval = Duration::from_secs(seconds);
                self.io.set_ppp_interval(new_interval);
                info!("PPP interval updated to {} seconds", seconds);
            }
        }
        Ok(())
    }
}

pub struct ModemIO<T: Read + Write + BufRead> {
    pub inner: T,
    state: ModemState,
    last_write: Option<Instant>,
    sms_check_interval: Duration,
    last_sms_check: Instant,
    new_sms_flag: AtomicBool,
    ppp_interval: Duration,
    last_ppp_session: Instant,
}
impl<T: Read + Write + BufRead> ModemIO<T> {
    fn new(inner: T) -> Self {
        Self {
            inner,
            state: ModemState::CommandMode,
            last_write: None,
            sms_check_interval: Duration::from_secs(30),
            last_sms_check: Instant::now(),
            new_sms_flag: AtomicBool::new(false),
            ppp_interval: Duration::from_secs(3600), // Default 1 hour
            last_ppp_session: Instant::now(),
        }
    }
    async fn ensure_command_mode(&mut self) -> Result<(), T::Error> {
        if self.state == ModemState::CommandMode {
            return Ok(());
        }

        // Send the escape sequence to exit data mode
        self.inner.write_all(b"+++").await?;
        Timer::after(Duration::from_secs(1)).await;
        self.state = ModemState::CommandMode;
        Ok(())
    }
    async fn enter_data_mode(&mut self) -> Result<(), T::Error> {
        self.inner.write_all(b"ATD*99***1#\r\n").await?;
        Timer::after(Duration::from_secs(1)).await;
        self.state = ModemState::DataMode;
        Ok(())
    }
    fn set_ppp_interval(&mut self, interval: Duration) {
        self.ppp_interval = interval;
    }
    async fn check_for_sms(&mut self) -> Result<(), T::Error> {
        self.ensure_command_mode().await?;
        self.state = ModemState::CheckingSMS;

        // Check for new SMS
        self.inner.write_all(b"AT+CMGL=\"REC UNREAD\"\r\n").await?;

        let mut response = String::new();
        let mut buf = [0u8; 128];
        loop {
            match self.inner.read(&mut buf).await {
                Ok(0) => break,
                Ok(n) => {
                    response.push_str(core::str::from_utf8(&buf[..n]).unwrap_or(""));
                    if response.contains("OK") {
                        break;
                    }
                }
                Err(e) => return Err(e),
            }
        }

        if response.contains("+CMGL:") {
            self.new_sms_flag.store(true, Ordering::Relaxed);
        }

        self.state = ModemState::CommandMode;
        self.last_sms_check = Instant::now();
        Ok(())
    }
}

#[derive(PartialEq)]
pub enum ModemState {
    CommandMode,
    DataMode,
    TransitioningToCommandMode,
    CheckingSMS,
}

// Add this custom error type and conversion:
#[derive(Debug)]
pub struct ModemError;

impl core::fmt::Display for ModemError {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "Modem error")
    }
}

impl core::convert::From<ModemError> for core::fmt::Error {
    fn from(_: ModemError) -> Self {
        core::fmt::Error
    }
}

/*
AT+CGATT=1 ;Attach to the GPRS network, can also use parameter 0 to detach.
OK ;Response, attach successful

AT+CGDCONT=? ;Input test command for help information.
+CGDCONT: (1..7), (IP,IPV6,PPP),(0..3),(0..4) OK ;Response, show the helpful information.

// AT+CGDCONT=1,"IP","internet"
AT+CGDCONT=1, "IP", "cmnet" ;Before active, use this command to set PDP context.
OK ;Response. Set context OK.

AT+CGACT=1,1 ;Active command is used to active the specified PDP context.
OK ;Response, active successful.

ATD*99***1# ;This command is to start PPP translation.
CONNECT ;Response, when get this, the module has been set to data state.
        PPP data should be transferred after this response and anything input is treated as data.
        i need to get into this mode when i need to make a http request and publish to an mqtt broker
        using reqwless crate and rust-mqtt crate respectively. So i need to be in COMMAND state mode
        as the default mode to be able to listen to incoming sms URCs and only shift to PPP mode or data state
        when i need to communicate to server(s). So i should be in COMMAND state most of the time and
        be in data state like three times in 24hrs.

+++ ;This command is to change the status to online data state.
    Notice that before input this command, you need to wait for a
    three seconds’ break, and it should also be followed by 3 seconds’
    break, otherwise “+++” will be treated as data.

ATH ;Use this command to return COMMAND state
ok Response
*/
